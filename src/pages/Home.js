import Jumbotron from '../components/cards/Jumbotron'
import { Container } from 'react-bootstrap'
import Banner from '../components/cards/Banner'
import Highlights from '../components/cards/Highlights'
import UserContext from '../UserContext';
import {useContext} from 'react'

export default function Home() {
  const {user} = useContext(UserContext);

  return (
    <div>
      <Jumbotron title="Hello dear customer" subtitle="Musical instrument store" />
      <Container>
        <Banner />
        <Highlights />
        
      </Container>
    </div>
  );
}

