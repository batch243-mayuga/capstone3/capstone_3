import Jumbotron from '../components/cards/Jumbotron'
import { Container, Col, Row, Form, Button } from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2'

import UserContext from '../UserContext';


export default function Login() {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [isActive, setIsActive] = useState(false);

  const {user, setUser} = useContext(UserContext);

  useEffect(() => {
    if(email !== '' && password !==''){
      setIsActive(true)
    } else{
      setIsActive(false)
    }
  }, [email, password])



  const loginUser = (event) => {
    event.preventDefault()

    fetch(`${process.env.REACT_APP_URI}/users/login`, {
      method: "POST",
      headers: {
        'Content-Type' : 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    }).then(res => res.json())
    .then(data => {
      console.log(data);

      if(data.accessToken !== 'empty'){
        localStorage.setItem('token', data.accessToken)
        retrieveUserDetails(data.accessToken);
        Swal.fire({
          title: 'Login Successful',
          icon: 'success',
          text: 'Welcome to our website!'
        })
      } else {
        Swal.fire({
          title: "Authentication Failed!",
          icon: 'error',
          text: 'Check your login details and try again.'
        })
        setPassword('')
      }
    })

    const retrieveUserDetails = (token) => {
      fetch(`${process.env.REACT_APP_URI}/users/profile`,
        {headers: {
          Authorization: `Bearer ${token} `
        }})
      .then(response => response.json())
      .then(data => {
        console.log(data);

        setUser({id: data._id, isAdmin: data.isAdmin});

        
      })
    }

  }


  return (
    (user.id !== null) ?
      <Navigate to="/" />
      :
    <div>
      <Jumbotron title="Login" />

      <Container>
      
        <Row>
          <Col className= "col-md-4 col-8 offset-md-4 offset-2">
            <Form onSubmit={loginUser} className=" p-3">
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={event => setEmail(event.target.value
                        )} required />
                    
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} onChange={event => setPassword(event.target.value
                        )} required />
                  </Form.Group>
                
                  <Button variant="success" type="submit" disabled={!isActive} >
                    Login
                  </Button>
                </Form>
            </Col>
          </Row >
        </Container>
    </div>
  );
}




