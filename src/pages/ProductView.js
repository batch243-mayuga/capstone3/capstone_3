import { useState, useEffect } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Jumbotron from '../components/cards/Jumbotron';

import Swal from 'sweetalert2';

export default function ProductView() {
	const [name, setName] = useState('');
	const [image, setImage] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [stock, setStock] = useState(0);

	const history = useNavigate()

	const {productId} = useParams();
	

	useEffect(() => {
		fetch(`${process.env.REACT_APP_URI}/products/${productId}`)
		.then(response => response.json())
		.then(data => {
			console.log(data)
			setName(data.name);
			setImage(data.image);
			setDescription(data.description);
			setPrice(data.price);
			setStock(data.stock);
		})


	}, [productId])


	const product = (productId) => {
		fetch(`${process.env.REACT_APP_URI}/products/${productId}`, {
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)
		
		})
	}

	return(
		<div>
		<Jumbotron title="Product Details" />
		<Container>
			<Row>
				<Col lg= {{span: 6, offset: 3}}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Card.Subtitle>Stocks</Card.Subtitle>
							<Card.Text>{stock}</Card.Text>
							<Button as={Link} to={`/cart/${productId}`} variant="primary" onClick = {()=> product(productId)} >Buy</Button>
						</Card.Body>	
					</Card>
					
				</Col>
			</Row>
		</Container>
		</div>
		)
}