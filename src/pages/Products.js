
import ProductCard from '../components/cards/ProductCard';
import Jumbotron from '../components/cards/Jumbotron';

import { Fragment, useEffect, useState } from 'react';

export default function Products() {

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_URI}/products/allProduct`)
			.then(response => response.json())
			.then(data => {
				let result = data?.map((product,index) => {
					return (<ProductCard key={index} productsProp={product}/>)
				})
				setProducts(result)
			})
	}, [])



	return (
		<Fragment>
			<Jumbotron title="Products" subtitle='Enjoy Shopping!' />
			{products}
		</Fragment>
	)
}