import { useState, useEffect } from 'react';
import Jumbotron from '../components/cards/Jumbotron'
import { Container, Col, Row , Form } from 'react-bootstrap'
import axios from 'axios'
import toast from 'react-hot-toast'

import { useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2'

export default function Register() {

  const [fullName, setFullName] = useState('');
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('')
  const [password2, setPassword2] = useState('')
  const [mobileNo, setMobileNo] = useState('')
  const [isActive, setIsActive]  = useState(false);


  const navigate = useNavigate();


  useEffect(()=> {
    // console.log(email);
    // console.log(password1);
    // console.log(password2);
    if(fullName !== '' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !== '' && password1 === password2){
        setIsActive(true);
    }else{
      setIsActive(false);
    }

  }, [fullName, email, mobileNo, password1, password2])


  function registerUser (event){
    event.preventDefault()

    fetch(`http://localhost:4000/users/register`, {
      method: 'POST',
      headers: {
        'Content-Type' : 'application/json'
      },
      body: JSON.stringify({
        fullName,
        email,
        mobileNo,
        password: password1
        
      })
    })
    .then(response => response.json())
    .then(data => {
      console.log(data)

      if(data.emailAlreadyExists){
        Swal.fire({
          title: 'Duplicate email found',
          icon: 'error',
          text: 'Please provide a different email.'
        })

        setPassword1('');
        setPassword2('');
      } else if(data.mobileNoLength === false){
        Swal.fire({
          title: 'Invalid Mobile Number',
          icon: 'error',
          text: 'Mobile number must be atleast 11 digits!'
        })

        setPassword1('');
        setPassword2('');
      } else{
        Swal.fire({
          title: "Registration successful",
          icon: 'success',
          text: 'Welcome to our website'
        })
         navigate('/login');
      }
    })
  }



  return (
    <div>
      <Jumbotron title="Register" />
        <Container className='mt-4'>
          <Row>
            <Col className='col-md-4 col-8 offset-md-4 offset-2'>

              <form onSubmit={registerUser}>

              <Form.Group className="mb-3">
                <Form.Label className='offset-md-4'>Full Name</Form.Label>
                <Form.Control type="text" placeholder="Enter your full name" value={fullName} onChange={event => setFullName(event.target.value
                    )} required />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label className='offset-md-4'>Email</Form.Label>
                <Form.Control type="text" placeholder="Enter your email" value={email} onChange={event => setEmail(event.target.value
                    )} required />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label className='offset-md-4'>Mobile Number</Form.Label>
                <Form.Control type="text" placeholder="Enter your mobile number" value={mobileNo} onChange={event => setMobileNo(event.target.value
                    )} required />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label className='offset-md-4'>Password</Form.Label>
                <Form.Control type="password" placeholder="Enter your password" value={password1} onChange={event => setPassword1(event.target.value
                    )} required />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label className='offset-md-4'>Password</Form.Label>
                <Form.Control type="password" placeholder="Confirm password" value={password2} onChange={event => setPassword2(event.target.value
                    )} required />
              </Form.Group>

              

              <button className='btn btn-success offset-md-4' type='submit' disabled={!isActive}>Register</button>
              </form>
            </Col>
          </Row>
        </Container>
    </div>
  );
}


{/*<input type='text' className='form-control mb-4 p-2' placeholder="Enter your full name" value={fullName} onChange={(e) => setFullName(e.target.value)} required />

              <input type='text' className='form-control mb-4 p-2' placeholder="Enter your email" value={email} onChange={(e) => setEmail(e.target.value)} required />

              <input type='text' className='form-control mb-4 p-2' placeholder="Enter your mobile number" value={mobileNo} onChange={(e) => setMobileNo(e.target.value)} required />

              <input type='password' className='form-control mb-4 p-2' placeholder="Enter your password" value={password1} onChange={(e) => setPassword1(e.target.value)} required />

              <input type='password' className='form-control mb-4 p-2' placeholder="Confirm password" value={password2} onChange={(e) => setPassword2(e.target.value)} required />*/}