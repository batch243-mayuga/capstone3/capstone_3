import Button from 'react-bootstrap/Button';
// Bootstrap Grid System
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import { Link } from 'react-router-dom';


export default function Banner() {

	return (
		<Row className='text-center'>
			<Col>
				
				<Button className='mt-4' as ={Link} to='/products' variant = 'primary'>GO TO PRODUCTS</Button>

			</Col>
		</Row>
		)
}