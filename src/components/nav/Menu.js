import { Container, Nav, Navbar } from 'react-bootstrap';
import { Fragment, useContext } from 'react'
import { NavLink } from 'react-router-dom';

import UserContext from '../../UserContext';

export default function Menu() {

	const {user} = useContext(UserContext);

	return(
		
			<Navbar bg="light" expand="lg" className="vw-100">
			      <Container fluid>
			        <Navbar.Brand as={NavLink} to="/" >HeavyMetal-Ins</Navbar.Brand>
			        <Navbar.Toggle aria-controls="basic-navbar-nav" />
			        <Navbar.Collapse id="basic-navbar-nav">
			          <Nav className="ms-auto">
			            
			            {
			            	(user.id !== null) 
			            	? (user.isAdmin === true)
			            	? <Fragment>
			            		<Nav.Link as={NavLink} to="/adminDashboard">Admin</Nav.Link>
			            		<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
			            	</Fragment>
			            	:
			            	<Fragment>
			            		<Nav.Link as={NavLink} to="/">Home</Nav.Link>
			            		<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
			            		{/* <Nav.Link as={NavLink} to="/cart">Cart</Nav.Link> */}
			            		<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
			            	</Fragment>
			            	: 
			            	<Fragment>
			            		<Nav.Link as={NavLink} to="/">Home</Nav.Link>
			            		<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
			            		<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
			            		<Nav.Link as={NavLink} to="/Login">Login</Nav.Link>
			            	</Fragment>
			            }

			          </Nav>
			        </Navbar.Collapse>
			      </Container>
			    </Navbar>
		)
}